const formAddPost = document.querySelector("#form-add-post");
const title = document.querySelector("#txt-title");
const body = document.querySelector("#txt-body");

const divPostEntries = document.querySelector("#div-post-entries");

let posts = [];
let id = 1;

// Add post data

// addEventListener(event, callback function that witll be triggere if the event occur or happen)
formAddPost.addEventListener("submit", (event) => {
  /*prevent Default function stops the auto reload of the webpage when submitting*/
  event.preventDefault();

  posts.push({
    id,
    title: title.value,
    body: body.value,
  });

  id++;
  console.log(posts);
  showPosts(posts);
});

// Show posts

const showPosts = (posts) => {
  let postEntries = "";

  posts.forEach((post) => {
    postEntries += `<div id = "post-${post.id}">
			<h3 id = "post-title-${post.id}">${post.title}</h3>
			<p id = "post-body-${post.id}">${post.body}</p>
			<button onclick = "editPost('${post.id}')" >Edit</button>
			<button onclick = "deletePost('${post.id}')">Delete</button>
			</div>`;
  });

  divPostEntries.innerHTML = postEntries;
};

// Edit post
const editPost = (id) => {
  let title = document.querySelector(`#post-title-${id}`).innerHTML;
  let body = document.querySelector(`#post-body-${id}`).innerHTML;

  document.querySelector("#txt-edit-title").value = title;
  document.querySelector("#txt-edit-body").value = body;
  document.querySelector("#txt-edit-id").value = id;
};

// Edit post
const deletePost = (id) => {
  posts.splice(id - 1, 1);
  const divDeleted = document.querySelector(`#post-${id}`);
  divDeleted.remove();
};

document
  .querySelector("#form-edit-post")
  .addEventListener("submit", (event) => {
    event.preventDefault();

    const idToBeEdited = document.querySelector("#txt-edit-id").value;

    for (let i = 0; i < posts.length; i++) {
      if (posts[i].id.toString() === idToBeEdited) {
        posts[i].title = document.querySelector("#txt-edit-title").value;
        posts[i].body = document.querySelector("#txt-edit-body").value;
        showPosts(posts);
        alert("Edit is successful");
        break;
      }
    }
  });
